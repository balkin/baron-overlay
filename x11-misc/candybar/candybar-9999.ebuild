# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5
DESCRIPTION="Candybar"
HOMEPAGE="https://github.com/Lokaltog/candybar/wiki/"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=">=x11-libs/gtk+-3.0.0
>=net-libs/webkit-gtk-2.4.0:3
>=dev-libs/jansson-2.5
"
RDEPEND=""

inherit git-r3

EGIT_REPO_URI="git://github.com/Lokaltog/candybar.git"

src_compile() {
	./waf configure --prefix=/usr
	./waf build
}

src_install() {
	./waf install --destdir="${D}"
}
